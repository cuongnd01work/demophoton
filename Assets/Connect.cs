using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon;
using Photon.Realtime;
public class Connect : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update
    private void Start()
    {
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        //base.OnConnectedToMaster();
        PhotonNetwork.JoinLobby();

    }
    public override void OnJoinedLobby()
    {
        //  base.OnJoinedLobby();
        Debug.Log("Lobby");
        PhotonNetwork.CreateRoom("zxc");
    }

}

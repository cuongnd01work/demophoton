using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class GameController2 : PunTurnManager
{
    private int id;
    [SerializeField] private TMP_Text turnText, timeText, textMessage, textPlayerCount;
    [SerializeField] private TMP_InputField inputMessage;
    [SerializeField] private Button btnSendMessage;
    [SerializeField] private GameObject findMatch;
    int i = 0;
    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.InRoom)
        {
            Debug.LogError("asdasd" + PhotonNetwork.CurrentRoom.Name);
            id = PhotonNetwork.CurrentRoom.PlayerCount - 1;
        }
        FunChangeTurn();
    }
    // Update is called once per frame
    void Update()
    {
        if (PhotonNetwork.InRoom)
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount == 4)
            {
                findMatch.SetActive(false);
                StopAllCoroutines();
                PhotonNetwork.CurrentRoom.IsOpen = false;
            }
            else if (PhotonNetwork.CurrentRoom.PlayerCount < 4)
            {
                findMatch.SetActive(true);
                PhotonNetwork.CurrentRoom.IsOpen = true;
            }
            else if (PhotonNetwork.CurrentRoom.PlayerCount > 4)
            {
                if (id >= 4)
                {
                    PhotonNetwork.LeaveRoom();
                }
            }
            textPlayerCount.text = PhotonNetwork.CurrentRoom.PlayerCount + " player:\n";

            if (PhotonNetwork.CurrentRoom.PlayerCount == 4)
            {
                if (i == 0 && Turn == 0)
                {
                    BeginTurn();
                    i++;
                }
                if (turnText != null)
                {
                    turnText.text = Turn + "";
                }

                if (Turn > 0 && timeText != null)
                {
                    timeText.text = Math.Round(RemainingSecondsInTurn, 0) + " s";
                }
                if (RemainingSecondsInTurn <= 0)
                {
                    BeginTurn();
                    FunChangeTurn();
                }
            }
        }
    }

    public void FunChatMessage()
    {
        PhotonView photonView = GetComponent<PhotonView>();
        textMessage.text += inputMessage.text + "\n";
        photonView.RPC("ChatMessage", RpcTarget.All, textMessage.text);
        inputMessage.text = "";

    }



    [PunRPC]
    public void ChatMessage(string text)
    {
        textMessage.text = text;
    }
    public void FunChangeTurn()
    {
        PhotonView photonView = GetComponent<PhotonView>();
        photonView.RPC("ChangeTurn", RpcTarget.All,"1t","2h","3o");
    }
    [PunRPC]
    public void ChangeTurn(string a1,string a2,string a3)
    {
        Debug.LogError("asd" + a1+"||" + a2 + "||" + a3 + "||");
        if (Turn % PhotonNetwork.CurrentRoom.PlayerCount == id)
        {
            //   btnSendMessage.onClick.AddListener(eventChangeTurn);
            btnSendMessage.gameObject.SetActive(true);
        }
        else
        {
            //     btnSendMessage.onClick.RemoveListener(eventChangeTurn);
            btnSendMessage.gameObject.SetActive(false);
        }
    }

    public void FunFindMatch()
    {
         PhotonView photonView = GetComponent<PhotonView>();
        photonView.RPC("FindMatch", RpcTarget.All);


    }
    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        PhotonNetwork.JoinRandomRoom(null, 4,MatchmakingMode.RandomMatching,null,null,null);
        PhotonNetwork.LoadLevel(SceneManagerHelper.ActiveSceneBuildIndex);
    }
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        id = PhotonNetwork.CurrentRoom.PlayerCount - 1;
    }
    [PunRPC]

    public void FindMatch()
    {
        StartCoroutine(waitMatch());
    }
    private IEnumerator waitMatch()
    {
        yield return new WaitForSeconds(0);//PhotonNetwork.CurrentRoom.PlayerCount * 10
        PhotonNetwork.LeaveRoom();
    }


}

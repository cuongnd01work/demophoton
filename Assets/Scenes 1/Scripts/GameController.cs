using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;
using Photon.Pun;
using Photon.Realtime;
using Photon.Chat;
using Photon.Pun.UtilityScripts;
using ExitGames.Client.Photon;
using System;
using TMPro;

public class GameController : MonoBehaviourPunCallbacks, IPunTurnManagerCallbacks
{
    private PunTurnManager punTurnManager;
    [SerializeField] private TMP_Text turnText,timeText;
    [SerializeField] private float TurnDuration;
    private void Start()
    {
        punTurnManager = gameObject.AddComponent<PunTurnManager>();
        punTurnManager.TurnManagerListener = this;
        punTurnManager.TurnDuration = TurnDuration;
    }
    private void Update()
    {
        if (PhotonNetwork.CurrentRoom.PlayerCount > 0&& punTurnManager.Turn==0)  
        {
            punTurnManager.BeginTurn();
           /* if (punTurnManager.IsOver)
            {
                return;
            }*/
        }

        if (turnText != null)
        {
            turnText.text = punTurnManager.Turn.ToString();
        }

        if (punTurnManager.Turn > 0 && timeText != null )
        {

            timeText.text = punTurnManager.RemainingSecondsInTurn + " SECONDS";

           // TimerFillImage.anchorMax = new Vector2(1f - punTurnManager.RemainingSecondsInTurn / punTurnManager.TurnDuration, 1f);
        }
    }

    #region TurnManagerCallbacks
    public void OnPlayerFinished(Player player, int turn, object move)
    {
        throw new NotImplementedException();
    }

    public void OnPlayerMove(Player player, int turn, object move)
    {
        throw new NotImplementedException();
    }

    public void OnTurnBegins(int turn)
    {
    }

    public void OnTurnCompleted(int turn)
    {
    }

    public void OnTurnTimeEnds(int turn)
    {
        throw new NotImplementedException();
    }
    #endregion
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;

public class ConnectToServer : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject panelLoading, panelJoinRoom;
    // Start is called before the first frame update
   private void Start()
    {
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
     //   base.OnConnectedToMaster();
        PhotonNetwork.JoinLobby();

    }
    public override void OnJoinedLobby()
    {
        panelLoading.SetActive(false);
        panelJoinRoom.SetActive(true);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon;

using Photon.Realtime;
using TMPro;

public class CreateAndJoinRoom : MonoBehaviourPunCallbacks
{
    public TMP_InputField createInput, joinInput;

    public void CreateRoom()
    {
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 4;
        PhotonNetwork.CreateRoom(createInput.text,roomOptions);
    }   
    public void JoinRoom()
    {
        PhotonNetwork.JoinRoom(joinInput.text);
    }
    public void JoinRoomRandom()
    {
        PhotonNetwork.JoinRandomOrCreateRoom();

    }
    public override void OnJoinedRoom()
    {
        PhotonNetwork.LoadLevel(SceneManagerHelper.ActiveSceneBuildIndex+1);
    }

}
